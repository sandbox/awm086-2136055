<?php

/**
 * @file
 * Chartbeat settings form.
 */

/**
 * Add a settings form for arwen configs.
 */
function chartbeat_blocks_admin_settings() {

  $form = array();

  // All we need to provide is the specific configuration options for our
  // block. Drupal will take care of the standard block configuration options
  // (block title, page visibility, etc.) and the save button.
  $form['chartbeat_blocks_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#description' => t('The domain name of the site you want to track.'),
    '#default_value' => variable_get('chartbeat_blocks_domain', ''),
  );

  $form['chartbeat_blocks_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Chartbeat API Key'),
    '#size' => 300,
    '#description' => t('Endpoint see https://chartbeat.com/docs/api/explore/'),
    '#default_value' => variable_get('chartbeat_blocks_api_key', ''),
  );

  // @TODO enable use to select
  $form['chartbeat_blocks_enabled'] = array(
    '#type' => 'checkboxes',
    '#options' => array('toppages' => 'Top Pages'),
    '#title' => t('Enable Blocks:'),
    '#default_value' => variable_get('chartbeat_blocks_enabled', array()),
  );
  // @TODO Establish a naming convention perhaps
  // chartbeat_blocks_chartbeat_METHODE_endpoint?
  $form['chartbeat_blocks_chartbeat_toppages_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Pages Endpoint'),
    '#size' => 300,
    '#description' => t('Endpoint see https://chartbeat.com/docs/api/explore/, Note that different endpoints may have different keys.'),
    '#default_value' => variable_get('chartbeat_blocks_chartbeat_toppages_endpoint', 'live/toppages/v3/'),
    '#states' => array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'invisible' => array(
        ':input[name="chartbeat_blocks_enabled[toppages]"]' => array('checked' => FALSE),
      ),
    ),
  );

  return system_settings_form($form);
}
